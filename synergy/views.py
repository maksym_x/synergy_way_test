from json import dumps
from datetime import date

from django.http import HttpResponse
from django.shortcuts import render
from django.template.context import Context
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, DetailView, CreateView, DeleteView

from synergy.db_wrapper import DBWrapper
from synergy.forms import CreateUser, UpdateUser


class UsersList(ListView):
    def dispatch(self, request, *args, **kwargs):
        self.db_wrapper = DBWrapper()
        return super(UsersList, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        per_page = request.GET.get('per_page')
        page = request.GET.get('page')
        if "username" in request.GET:
            data = self.db_wrapper.select_many("users", [("name", request.GET["username"])])
        else:
            data = self.db_wrapper.select_many("users")
        if per_page and page:
            start = int(page) * int(per_page)
            finish = int(page) * int(per_page) + int(per_page)
            data = data[start:finish]
        return render(request, "users_list.html", context=Context({"data": data}), content_type='text/html')


class UserDetails(DetailView):
    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(UserDetails, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        db_wrapper = DBWrapper(keep_conn=True)
        courses = db_wrapper.select_many("courses")

        db_data = db_wrapper.select_one("users", [("id", kwargs["user_id"])])
        update_form = UpdateUser(db_data)
        user_courses_ids = [u_c['course_id'] for u_c in db_wrapper.select_many("user_course",
                                                                               [('user_id', kwargs["user_id"]),
                                                                                ('available', False)])]
        db_wrapper.db_connect.close()
        return render(request, "user_detail.html", content_type='text/html',
                      context=Context({"courses": courses,
                                       "user_form": update_form,
                                       "user_courses": user_courses_ids,
                                       "user_id": db_data["id"]}))

    def post(self, request, *args, **kwargs):
        db_wrapper = DBWrapper(keep_conn=True)
        courses = db_wrapper.select_many("courses")

        update_form = UpdateUser(request.POST)
        if update_form.is_valid():
            update_items = [u for u in update_form.data.items() if u[0] != 'csrfmiddlewaretoken']

            db_wrapper.update_one('users', [("id", kwargs["user_id"])], update_items)
            user_courses_ids = [u_c['course_id'] for u_c in db_wrapper.select_many("user_course",
                                                                                   [('user_id', kwargs["user_id"]),
                                                                                    ('available', False)])]
            db_wrapper.db_connect.close()
            return render(request, "user_detail.html",
                          context=Context({"courses": courses,
                                           "user_form": update_form,
                                           "user_courses": user_courses_ids,
                                           "successful": "Successful"}),
                          content_type='text/html')


class UsersCreate(CreateView):
    def dispatch(self, request, *args, **kwargs):
        self.db_wrapper = DBWrapper()
        return super(UsersCreate, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        create_form = CreateUser()
        return render(request, "user_create.html", context=Context({"user_form": create_form}),
                      content_type='text/html')

    def post(self, request, *args, **kwargs):
        create_form = CreateUser(request.POST)
        if create_form.is_valid():
            insert_items = [i for i in create_form.data.items() if i[0] != 'csrfmiddlewaretoken']
            insert_items.append(('date_created', date.today()))

            self.db_wrapper.insert_one('users', insert_items)
            return render(request, "user_create.html", context=Context({"user_form": create_form, "successful": True}),
                          content_type='text/html')


class UserDelete(DeleteView):
    def dispatch(self, request, *args, **kwargs):
        self.db_wrapper = DBWrapper(keep_conn=True)
        return super(UserDelete, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        user_id = request.POST["user_id"]
        self.db_wrapper.delete_one('users', [("id", user_id)])
        self.db_wrapper.delete_one('user_course', [("user_id", user_id)])

        self.db_wrapper.db_connect.close()
        return HttpResponse(dumps({"status": "OK"}), status=200, content_type='application/json')


class CoursesList(ListView):
    def dispatch(self, request, *args, **kwargs):
        self.db_wrapper = DBWrapper()
        return super(CoursesList, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        data = self.db_wrapper.select_many("courses")
        return render(request, "courses_list.html", context=Context({"data": data}),
                      content_type='text/html')


class UsersCoursesDetail(DetailView):
    def dispatch(self, request, *args, **kwargs):
        self.db_wrapper = DBWrapper(keep_conn=True)
        return super(UsersCoursesDetail, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        user_id = request.POST["user_id"]
        course_id = request.POST["course_id"]
        available = request.POST["available"]
        user_course = self.db_wrapper.select_one('user_course', [("user_id", user_id),
                                                                 ("course_id", course_id)])
        if user_course:
            self.db_wrapper.update_one('user_course', [("user_id", user_id),
                                                       ("course_id", course_id)],
                                       [("available", available)])
        else:
            self.db_wrapper.insert_one('user_course', [("user_id", user_id),
                                                       ("course_id", course_id),
                                                       ("available", available)])
        self.db_wrapper.db_connect.close()
        return HttpResponse(dumps({"status": "OK"}), status=200, content_type='application/json')
