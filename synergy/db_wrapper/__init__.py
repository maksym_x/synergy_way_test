import psycopg2
from psycopg2.extras import RealDictCursor
from django.conf import settings


class DBWrapper(object):
    def __init__(self, keep_conn=False):
        self.keep_conn = keep_conn
        self.db_connect = psycopg2.connect(
            database=settings.DATABASES['default']['NAME'],
            user=settings.DATABASES['default']['USER'],
            password=settings.DATABASES['default']['PASSWORD'],
            host=settings.DATABASES['default']['HOST'],
            port=settings.DATABASES['default']['PORT'],
            cursor_factory=RealDictCursor
        )
        self.db_connect.set_isolation_level(0)

    def insert_one(self, table, data):
        fields = ", ".join([item[0] for item in data])
        values = ", ".join(['\'%s\'' % item[1] for item in data])

        cursor = self.db_connect.cursor()
        cursor.execute("INSERT INTO {table_n} ({fields}) VALUES ({values});" \
                       .format(table_n=table, fields=fields, values=values))
        self.db_connect.commit()
        if not self.keep_conn:
            self.db_connect.close()

    def update_one(self, table, condition, data):
        fields_values = ", ".join(["%s='%s'" % (field, value) for field, value in data])
        conditions = "AND ".join(["%s='%s'" % (field, value) for field, value in condition])

        cursor = self.db_connect.cursor()
        cursor.execute("""
                       UPDATE {table_n}
                       SET {fields_values}
                       WHERE {conditions};
                       """.format(table_n=table, fields_values=fields_values, conditions=conditions))
        self.db_connect.commit()
        if not self.keep_conn:
            self.db_connect.close()

    def update_bulk(self):
        pass

    def delete_one(self, table, condition):
        conditions = "AND ".join(["%s='%s'" % (field, value) for field, value in condition])
        cursor = self.db_connect.cursor()
        cursor.execute("DELETE FROM {table_n} WHERE {conditions};".format(table_n=table, conditions=conditions))
        self.db_connect.commit()
        if not self.keep_conn:
            self.db_connect.close()

    def select_one(self, table, condition):
        conditions = "AND ".join(["%s='%s'" % (field, value) for field, value in condition])
        cursor = self.db_connect.cursor()
        cursor.execute(
            "SELECT * FROM {table_n} WHERE {conditions} LIMIT 1;".format(table_n=table, conditions=conditions))
        result = cursor.fetchone()
        if not self.keep_conn:
            self.db_connect.close()
        return result

    def select_many(self, table, condition=None):
        if condition:
            conditions = "AND ".join(["%s='%s'" % (field, value) for field, value in condition])
            query = "SELECT * FROM {table_n} WHERE {conditions};".format(table_n=table, conditions=conditions)
        else:
            query = "SELECT * FROM {table_n};".format(table_n=table)

        cursor = self.db_connect.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        if not self.keep_conn:
            self.db_connect.close()
        return result
