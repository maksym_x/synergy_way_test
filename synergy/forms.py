from django import forms


class CreateUser(forms.Form):
    custom_errors = {'required': 'This field is required', 'invalid': 'Enter a valid value'}
    email_errors = {'required': 'This field is required', 'invalid': 'Pleas enter a valid email address'}
    STATUS_CHOICES = (
        (False, 'Inactive'),
        (True, 'Active'),
    )
    name = forms.CharField(max_length=255, required=True, label='Name', error_messages=custom_errors,
                           widget=forms.TextInput(attrs={'class': 'form-control custom-usr-input'}))
    email = forms.EmailField(required=True, error_messages=email_errors,
                             widget=forms.TextInput(attrs={'class': 'form-control custom-usr-input'}))
    phone = forms.CharField(required=False, error_messages=custom_errors,
                            widget=forms.TextInput(attrs={'class': 'form-control custom-usr-input',
                                                          'type': 'tel', 'name': 'country', 'size': '4'}))
    mobile = forms.CharField(required=False, error_messages=custom_errors,
                             widget=forms.TextInput(attrs={'class': 'form-control custom-usr-input'}))
    status = forms.ChoiceField(required=False, choices=STATUS_CHOICES, error_messages=custom_errors,
                               widget=forms.Select(attrs={'class': 'form-control custom-select-cr-users'}))

    class Meta:
        fields = '__all__'


class UpdateUser(forms.Form):
    custom_errors = {'required': 'This field is required', 'invalid': 'Enter a valid value'}
    email_errors = {'required': 'This field is required', 'invalid': 'Pleas enter a valid email address'}
    STATUS_CHOICES = (
        (False, 'Inactive'),
        (True, 'Active'),
    )
    name = forms.CharField(max_length=255, required=True, label='Name', error_messages=custom_errors,
                           widget=forms.TextInput(attrs={'class': 'form-control custom-usr-input',
                                                         'readonly': 'readonly'}))
    email = forms.EmailField(required=True, widget=forms.TextInput(attrs={'class': 'form-control custom-usr-input'}),
                             error_messages=email_errors)
    phone = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'form-control custom-usr-input',
                                                                          'type': 'tel', 'name': 'country',
                                                                          'size': '4'}), error_messages=custom_errors)
    mobile = forms.CharField(required=False, widget=forms.TextInput(attrs={'class': 'form-control custom-usr-input'}),
                             error_messages=custom_errors)
    status = forms.ChoiceField(required=False, choices=STATUS_CHOICES, error_messages=custom_errors,
                               widget=forms.Select(attrs={'class': 'form-control custom-select-cr-users'}))

    class Meta:
        fields = '__all__'
