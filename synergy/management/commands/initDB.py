from synergy.db_wrapper import DBWrapper
from django.core.management import BaseCommand


class Command(BaseCommand):
    args = ''
    help = "Initiate database"

    def handle(self, *args, **options):
        db_wrapper = DBWrapper()
        db_connect = db_wrapper.db_connect

        cursor = db_connect.cursor()
        cursor.execute("""
        CREATE TABLE IF NOT EXISTS Users (
          id           SERIAL PRIMARY KEY,
          name         VARCHAR(255) NOT NULL,
          email        VARCHAR(255) UNIQUE,
          phone        VARCHAR(30)  NOT NULL,
          mobile       VARCHAR(30)  NOT NULL,
          status       BOOLEAN,
          date_created DATE
        );
        """)
        cursor.execute("""
        CREATE TABLE IF NOT EXISTS Courses (
          id           SERIAL PRIMARY KEY,
          code         VARCHAR(7) UNIQUE,
          name         VARCHAR(255) NOT NULL
        );
        """)
        cursor.execute("""
        CREATE TABLE IF NOT EXISTS user_course (
          user_id    INT REFERENCES Users (id) ON UPDATE CASCADE ON DELETE CASCADE,
          course_id INT REFERENCES Courses (id) ON UPDATE CASCADE,
          available    BOOLEAN,
          CONSTRAINT user_course_pkey PRIMARY KEY (course_id, user_id)  -- explicit pk
        );
        """)

        cursor.execute("""
        CREATE INDEX name_ind ON Users (name);
        CREATE INDEX email_ind ON Users (email);
        CREATE INDEX id_ind ON Courses (id);
        CREATE INDEX code_ind ON Courses (code);
        CREATE INDEX course_name_ind ON Courses (name);
        """)

        # INITIAL DATA
        cursor.execute("""
        INSERT INTO users (id, name, email, phone, mobile, status, date_created) VALUES
        (1, 'Jim', 'bla@bla.bla', '123', '+380931234568', FALSE, '2016-08-13'),
        (2, 'Pete', 'bla2@bla.bla', '+380931234567', '+380931234568', TRUE, '2016-08-13');

        INSERT INTO courses (id, code, name) VALUES
        (1, 'P012345', 'Python-Base'),
        (2, 'P234567', 'Python-Database'),
        (3, 'H345678', 'HTML'),
        (4, 'J456789', 'Java-Base'),
        (5, 'JS54321', 'JavaScript-Base');
        """)

        db_connect.commit()

        print("Database has been created")
