from django.conf.urls import url
from synergy.views import UsersList, CoursesList, UserDetails, UsersCoursesDetail, UsersCreate, UserDelete

urlpatterns = [
    url(r'^$', UsersList.as_view(), name="users"),
    url(r'^users/create/$', UsersCreate.as_view(), name="user_create"),

    url(r'^user/(?P<user_id>\d+)/$', UserDetails.as_view(), name="user_details"),

    url(r'^user/delete/$', UserDelete.as_view(), name="user_delete"),

    url(r'^courses/$', CoursesList.as_view(), name="courses"),

    url(r'^user_courses/$', UsersCoursesDetail.as_view(), name="user_courses")
]
